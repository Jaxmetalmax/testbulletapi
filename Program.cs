﻿using System;
using System.Collections.Generic;

namespace TestBulletAPI
{
	class MainClass
	{
		private static string pvkey = "yourkeygoeshere";
		private static Dictionary<string,string> devices;
		private static List<string> iddev = new List<string> ();

		public static void Main (string[] args)
		{
			Console.WriteLine ("Inserta tu API key de PushBullet y presiona enter:");
			pvkey=Console.ReadLine ();
			Console.WriteLine ("Lista de dispositivos:");
			muestraDispositivos ();
			Console.WriteLine ("Escoja un dispositivoa enviar mensaje y presione 'enter' (0,1,2,n...):");
			var dev = Console.ReadLine();
			enviaMsj (dev, "Mensaje de prueba", "Titulo mensaje test");
			Console.WriteLine ("Mensaje enviado");

		}

		public static void muestraDispositivos()
		{
			var numDev = 0;
			devices = BulletAPI.BulletAPI.GetDevices (true, pvkey);
			foreach (KeyValuePair<string, string> dev in devices) 
			{
				Console.WriteLine ("device {0}: {1}", numDev,dev.Value);
				iddev.Add (dev.Key);
				numDev++;
			}
		}

		public static void enviaMsj(string dev, string mensaje, string titulo)
		{
			var deviceid = iddev [Convert.ToInt32(dev)];
			BulletAPI.BulletAPI.PushIt (false, pvkey, "note", deviceid, mensaje,titulo);
		}
	}
}
